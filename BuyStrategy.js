"use strict";

// Import the module 
var polo = require("poloniex-unofficial");

// User Input
var APIKey = "API_KEY_HERE";
var APISecret = "API_SECRET_HERE";
var symbolName = "XMR";
var baseCurrency = "USDT";

// Get access to the trading API 
var poloTrading = new polo.TradingWrapper(APIKey, APISecret);
var poloPublic = new polo.PublicWrapper();

// Global variables
var last_Position;
var avalibleBaseCurrency;
var avalibleSymbolCurrency;
var onOrderSymbolCurrency;
var currentPrice;
var highOneBarAgo;
var highTwoBarsAgo;
var highestHigh;
var lowOneBarAgo;
var lowTwoBarsAgo;
var lowestLow;
var orderAmount;

// Get complete balances
function getCompleteBalances() {  
    var deferred = Promise.defer();
    // Receive Complete Balances 
    poloTrading.returnCompleteBalances("all", (err, response) => {
        if (err) {
            // Log error message 
            console.log("An getCompleteBalances error occurred: " + err.msg);
            // Disconnect 
            return true;
        }
        deferred.resolve(response);
    }); 
    return deferred.promise;
}

function getChartData() {  
    var deferred = Promise.defer();    
    var periode = 14400;
    var StratDate = (Date.now() / 1000) - (periode * 3);
    var EndDate = Date.now() / 1000;

    // Return highest high on a 240 min chart 
    poloPublic.returnChartData(baseCurrency + "_" + symbolName, StratDate, EndDate, periode, (err, response) => {
        if (err) {
            // Log error message 
            console.log("An returnChartData error occurred: " + err.msg);
            // Disconnect 
            return true;
        }        
        deferred.resolve(response);
    });   
    // Return chart data response 
    return deferred.promise;
}

// 
function sendBuyOrder(currentPrice, orderAmount) {  
    var deferred = Promise.defer();
    
    poloTrading.buy(baseCurrency + "_" + symbolName, currentPrice, orderAmount, 0, 0 , 0, (err, response) => {
         if (err) {
             // Log error message 
             console.log("An buy error occurred: " + err.msg); 
             // Disconnect 
             return true;
         }  
        last_Position = "buy";
        console.log(response);           
     }); 
    // Return buy order response 
    return deferred.promise;
}

// 
function sendSellOrder(currentPrice, avalibleSymbolCurrency) {  
    var deferred = Promise.defer();
    
    poloTrading.sell(baseCurrency + "_" + symbolName, currentPrice, avalibleSymbolCurrency, 0, 0 , 0, (err, response) => {
         if (err) {
             // Log error message 
             console.log("An sell error occurred: " + err.msg); 
             // Disconnect 
             return true;
         }  
        last_Position = "sell";
        console.log(response);  
     }); 
    // Return buy order response 
    return deferred.promise;
}

var runBot = function() {  
    Promise.all([getCompleteBalances(), getChartData()])
    .then((values) => {
        // Store variables
        avalibleBaseCurrency = values[0][baseCurrency].available;
        avalibleSymbolCurrency = values[0][symbolName].available;
        onOrderSymbolCurrency = values[0][symbolName].onOrders;
        currentPrice = values[1][2].close;
        console.log("Avalible " + baseCurrency + " = " + avalibleBaseCurrency);      
        console.log("Avalible " + symbolName + " = " + avalibleSymbolCurrency);    
        console.log("On order " + symbolName + " = " + onOrderSymbolCurrency);  
        console.log('Current price = ' + currentPrice);
        
        if (avalibleBaseCurrency > 0 && last_Position !== "buy" && avalibleSymbolCurrency == 0 && onOrderSymbolCurrency == 0) {
            console.log('Look for a buy signal');          
            highOneBarAgo = values[1][1].high;
            highTwoBarsAgo = values[1][0].high;
            console.log('High one bar ago = ' + highOneBarAgo);  
            console.log('High two bars ago = ' + highTwoBarsAgo);  

            if (highOneBarAgo > highTwoBarsAgo) {
                highestHigh = highOneBarAgo;
            } else {
                highestHigh = highTwoBarsAgo;
            }
            console.log('Highest high = ' + highestHigh);            
            
            // Calculate order amount
            orderAmount = avalibleBaseCurrency / currentPrice;           
            console.log('Order amount = ' + orderAmount);
        
            // calc high high
            // Buy signal function
            if (currentPrice > highestHigh && avalibleBaseCurrency > 0 && avalibleSymbolCurrency == 0 ) {
                sendBuyOrder(currentPrice, orderAmount);
                console.log('A buy order has been send'); 
            }
        } else if (avalibleSymbolCurrency > 0) {      
            console.log('Look for a sell signal'); 
            lowOneBarAgo = values[1][1].low;
            lowTwoBarsAgo = values[1][0].low; 
            if (lowOneBarAgo < lowTwoBarsAgo) {
                lowestLow = lowOneBarAgo;
            } else {
                lowestLow = lowTwoBarsAgo;
            }
            console.log('Low one bar ago = ' + lowOneBarAgo);  
            console.log('Low two bars ago = ' + lowTwoBarsAgo); 
            console.log('Lowest low = ' + lowestLow);
             // calc low low
            if (currentPrice < lowestLow && avalibleSymbolCurrency > 0) {
                sendSellOrder(currentPrice, avalibleSymbolCurrency);
                console.log('A sell order has been send'); 
            }   
        }
    })
    .catch((error) => {
        console.log(error.message);
    })
}

function Loop() {
    // Example based on https://gist.github.com/kumatch/5234434
    var fs = require('fs');
    var INTERVAL = 5000; // 5 sec 
    var cycle_stop = false;
    var daemon = false;
    var timer;
    process.argv.forEach(function (arg) {
        if (arg === '-d')
            daemon = true;
    });
    process.on('SIGTERM', function () {
        console.log('Got SIGTERM signal.');
        stop();
    });
    (function cycle() {
        timer = setTimeout(function () {
            runTask();
            if (!cycle_stop)
                cycle();
        }, INTERVAL);
    })();
    function runTask() {
        runBot();        
    }
    function stop() {
        cycle_stop = true;
        clearTimeout(timer);
    }
    if (daemon) {
        require('daemon')();
    }
}

Loop();